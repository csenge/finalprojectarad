package edu.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalProjectAradRunner {

    public static void main(String[] args) {
        SpringApplication.run(FinalProjectAradRunner.class);
    }
}
