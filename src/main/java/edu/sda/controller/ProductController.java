package edu.sda.controller;

import edu.sda.model.Product;
import edu.sda.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class ProductController {

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping(value = "/insertProduct")
    public void insertProduct(@RequestBody Product product) {
        productService.insertProduct(product);
    }

    // when using get without an URI this method will be called
    @GetMapping(value = "/")
    public List<Product> getAllProducts() {
        return productService.getAllProducts();
    }

    @GetMapping(value = "/getById")
    public Product getProductById(@RequestParam Long id) {
        return productService.getProductById(id);
    }

    //@DeleteMapping

    //@PutMapping
}
