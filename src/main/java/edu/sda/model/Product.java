package edu.sda.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    /**
     * https://www.uuidgenerator.net
     * UUID = Universally Unique IDs, use these in order to avoid overflows for ids
     * for ex. if you want to have more ids than the value of MAX_INT
     * UUID ex: 315b664b-074a-44a4-9c55-3a3335c8a79c
     */
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private Double price;

    private Date expirationDate;

//    private Blob image; todo

    /**
     In order to use this model entity as request body we will have to use its JSON representation,

     for example:
     {
        "name": "lapte",
        "description": "lapte Napolact",
        "price": 3,5
     }
     */
}
