package edu.sda.service;

import edu.sda.model.Product;
import edu.sda.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {

    //Autowiring on field - not really used, Spring teams suggest using setter based wiring or constructor based wiring over field autowiring
   // @Autowired
    private ProductRepository productRepository;

    //constructor based wiring - used for fields that are mandatory to be wired
//    public ProductService(ProductRepository productRepository) {
//        this.productRepository = productRepository;
//    }

    //Autowiring on setter
    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void insertProduct(Product product) {
        productRepository.save(product);
    }


    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product getProductById(Long id) {
        Optional<Product> productOptional = productRepository.findById(id);

        System.out.println("I am searching");

        if (productOptional.isPresent()) {
            return productOptional.get();
        } else {
            return null;
            // throw product not found exception
        }
    }
}
