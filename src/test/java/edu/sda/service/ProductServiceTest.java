package edu.sda.service;

import edu.sda.model.Product;
import edu.sda.repository.ProductRepository;
import org.junit.Test;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProductServiceTest {

    @InjectMocks
    private ProductService productService;

    @Mock
    private ProductRepository productRepository;

    @Before
    public void setup() {
        System.out.println("before method");
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllProductsTest() {
        //GIVEN
        List<Product> expectedResult = new ArrayList<>();
        when(productRepository.findAll()).thenReturn(expectedResult);

        //WHEN
        List<Product> actualResult = productService.getAllProducts();

        //THEN
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void insertProductTest() {
        //given
        // create real object and use this in the test
        // - OPTION 1 for creating a paramter for the insertProduct method from ProductService
        Product productToBeInserted = new Product();
        //OPTION 2
        // create a mock to be used as a parameter
        Product productToBeInsertedMock = mock(Product.class);

        when(productRepository.save(any())).thenReturn(productToBeInserted);


        //when
        //OPTION 3 - use any() - to insert any object
        productService.insertProduct(any());

        //then - if left empty - verify there is no exception
    }

    @Test
    public void getProductByIdTest() {
        //given
        Product productFound = new Product();
        Optional<Product> productOptional = Optional.of(productFound);
        when(productRepository.findById(1L)).thenReturn(productOptional);

        //when
        Product actualResult = productService.getProductById(1L);

        //then
        assertEquals(productFound, actualResult);
    }

    @Test
    public void getProductByIdTest_noProductFound() {
        //given
        Optional<Product> productOptional = Optional.empty();
        when(productRepository.findById(1L)).thenReturn(productOptional);

        //when
        Product actualResult = productService.getProductById(1L);

        //then
        assertNull(actualResult);
    }
}
